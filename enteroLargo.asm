crearEnteroLargo:
	add		$v0, $gp, $zero			#Save the structure base address (the return value)
	addi		$gp, $gp, 260				#Reserve space for the struct
	sw		$zero, 256($v0)			#Set tamano to 0
	jr		$ra
sumarEnterosLargos:
	lw		$t0, 256($a0)				#Load tamano of operando1
	lw		$t1, 256($a1)				#Load tamano of operando2
	or		$t2, $t0, $t1				#To verify if both tamanos are 0
if1:	bne		$t2, $zero, else1
	sw		$zero, 256($a2)			#As operando1 and operando2 are 0, return 0
	j		exitIf1
else1:
	slt		$t2, $t0, $t1
if2:	beq		$t2, $zero, exitIf2			#Calculate the number of iterations needed ($t0 <- iterations)
	add		$t0, $t1, $zero
exitIf2:
	and		$t1, $zero, $zero			#Initialize $t1 in 0 (i <- 0)
	and		$t2, $zero, $zero			#Initialize $t2 in 0 (acarreo <- 0)
	addi		$t7, $zero, 62				#Set the maximum number of iterations
while:
	beq		$t1, $t7, endWhile
	bne		$t2, $zero, overflow			#If there is acarreo, don't check the maximum number of iterations
	bge		$t1, $t0, endWhile
overflow:
	sll		$t8, $t1, 2
	add		$t9, $t8, $a0
	lw		$t3, 0($t9)				#Load operando1[i]
	add		$t9, $t8, $a1
	lw		$t4, 0($t9)				#Load operando2[i]
if3:	beq		$t2, $zero, exitIf3			#Check if the acarreo is 0
	not		$t5, $t2
	addiu	$t5, $t5, 1				#Calculate Two's complement
	sgeu		$t9, $t3, $t5
	addu	$t3, $t3, $t2				#operando1[i] += acarreo
if4:	bne		$t9, $zero, exitIf4			#operando1[i] >= acarreo Two's complement
	and		$t2, $zero, $zero
exitIf4:
exitIf3:
if5:	beq		$t4, $zero, exitIf5			#Check if the operando2[i] is 0
	not		$t5, $t4
	addiu	$t5, $t5, 1				#Calculate Two's complement
	sgeu		$t9, $t3, $t5
	addu	$t3, $t3, $t4				#operando1[i] += operando2[i]
if6:	beq		$t9, $zero, exitIf6			#operando1[i] >=  operando2[i] Two's complement
	addi		$t2, $zero, 1
exitIf6:
exitIf5:
	add		$t9, $t8, $a2
	sw		$t3, 0($t9)				#Store resultado[i]
	addi		$t1, $t1, 1				# i++
	j		while
endWhile:
	sw		$t1, 256($a2)				#Set the tamano of the result
exitIf1:
	jr		$ra
exit:
